
const FIRST_NAME = "POPA";
const LAST_NAME = "RARES-MIHAI";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
class Employee {
    constructor(name , surname , salary){
        this.name=name;
        this.surname=surname;
        this.salary=salary;
    }
    getDetails(){
        return this.name +" "+ this.surname +" "+ this.salary;
    }
}

class SoftwareEngineer extends Employee {
   constructor(name, surname, salary, experience="JUNIOR"){
       super(name,surname,salary);
       this.experience=experience
   }
   applyBonus(){
       if(this.experience.toUpperCase()==="JUNIOR")
       return this.salary*1.1;
       else if(this.experience.toUpperCase()==="MIDDLE")
       return this.salary*1.15;
       else if(this.experience.toUpperCase()==="SENIOR")
       return this.salary*1.2;
       else
       return this.salary*1.1;

   }
}
// var soft=new SoftwareEngineer("Eu","Sunt",20,"MASTER");
// var softer=new SoftwareEngineer("Eu","Sunt",20,);
// console.log(soft);
// console.log(softer);
//var pers=new SoftwareEngineer("John","Doe",1000,"JUNIOR");



module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

